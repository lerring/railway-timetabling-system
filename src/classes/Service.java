package classes;

import java.util.ArrayList;

public class Service {

	private String serviceID;
	private Calendar calender;
	private ArrayList<CalendarDate> datesWithExceptions = new ArrayList<CalendarDate>();
	private ArrayList<Trip> tripList = new ArrayList<Trip>();

	public Service(String serviceID){
		setServiceID(serviceID);
	}
	
	public ArrayList<Trip> getTripList(){
		return tripList;
	}

	public boolean addTrip(Trip tripToAdd) {
		return tripList.add(tripToAdd);
	}
	
	public Calendar getCalender() {
		return calender;
	}


	public void setCalender(Calendar calender) {
		this.calender = calender;
	}


	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}
	
	public boolean addCalendarDate(CalendarDate cd) {
		return datesWithExceptions.add(cd);
	}
	
	public ArrayList<CalendarDate> getDatesWithExceptions() {
		return datesWithExceptions;
	}

	public void setDatesWithExceptions(ArrayList<CalendarDate> datesWithExceptions) {
		this.datesWithExceptions = datesWithExceptions;
	}
	
}
