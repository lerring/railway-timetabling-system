package classes;

import system.Console;

public class Stop {

	private String stopId;
	private String stopCode;
	private String stopName;
	private String stopLat;
	private String stopLong;
	private String stopUrl;

	public Stop(String fileString) {

		String[] split = Console.parseCSVLine(fileString);
		setStopId(split[0]);
	//	setStopCode(split[1]);
		setStopName(split[1]);
		setStopLat(split[2]);
		setStopLong(split[3]);
		setStopUrl(split[4]);

	}


	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	public String getStopCode() {
		return stopCode;
	}

	public void setStopCode(String stopCode) {
		this.stopCode = stopCode;
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public String getStopLat() {
		return stopLat;
	}

	public void setStopLat(String stopLat) {
		this.stopLat = stopLat;
	}

	public String getStopLong() {
		return stopLong;
	}

	public void setStopLong(String stopLong) {
		this.stopLong = stopLong;
	}

	public String getStopUrl() {
		return stopUrl;
	}

	public void setStopUrl(String stopUrl) {
		this.stopUrl = stopUrl;
	}

}
