package classes;

import java.util.ArrayList;

public class Route {

	private ArrayList<Service> serviceList = new ArrayList<Service>();
	private ArrayList<Trip> tripList = new ArrayList<Trip>();
	private String routeID;
	private String agencyID;
	private String shortName;
	private String longName;
	private int type;

	public Route(String fileString) {
		String[] split = fileString.split(",");
		setRouteID(split[0]);
		setAgencyID(split[1]);
		setShortName(split[2]);
		setLongName(split[3]);
		setType(split[4]);
	}

	public Route(String routeID, String agencyID) {
		setRouteID(routeID);
		setAgencyID(agencyID);
	}

	public Route(String routeID, String agencyID, String shortName, String longName) {
		this(routeID, agencyID);
		setShortName(shortName);
		setLongName(longName);
	}

	public Route(String routeID, String agencyID, String shortName, String longName, int type) {
		this(routeID, agencyID, shortName, longName);
		setType(type);
	}

/////////////
// Methods //
/////////////

	public ArrayList<Trip> getTripList() {
		return tripList;
	}

	public ArrayList<Service> getServiceList() {
		return serviceList;
	}

	public void setServiceList(ArrayList<Service> serviceList) {
		this.serviceList = serviceList;
	}

	public boolean addService(Service serviceToAdd) {
		System.out.println("soi");
		return serviceList.add(serviceToAdd);
	}

	public void setTripList(ArrayList<Trip> tripList) {
		this.tripList = tripList;
	}

	public boolean addTrip(Trip tripToAdd) {
		return tripList.add(tripToAdd);
	}

	public String getRouteID() {
		return routeID;
	}

	public void setRouteID(String routeID) {
		if (routeID != null)
			this.routeID = routeID;
	}

	public String getAgencyID() {
		return agencyID;
	}

	public void setAgencyID(String agencyID) {
		if (agencyID != null)
			this.agencyID = agencyID;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		if (shortName != null)
			this.shortName = shortName;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		if (longName != null)
			this.longName = longName;
	}

	public int getType() {
		return type;
	}

	public void setType(String typeAsString) {
		if (typeAsString == null)
			return;

		if (typeAsString.equals(""))
			return;

		setType(Integer.parseInt(typeAsString));

	}

	public void setType(int type) {
		this.type = type;
	}

}
