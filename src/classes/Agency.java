package classes;

import java.util.ArrayList;
import java.util.HashMap;

public class Agency {

	private String ID, name, URL, timeZone, language, phoneNum, fareURL, email;
	private ArrayList<Route> routeList = new ArrayList<Route>();

	//////////////////
	// Constructors //
	//////////////////
	
	public Agency(String fileString){
		String[] split = fileString.split(",");
		setID(split[0]);
		setName(split[1]);
		setURL(split[2]);
		setLanguage(split[3]);
	}
	
	//Basic Constructor
	public Agency(String ID, String name, String URL) {
		setID(ID);
		setName(name);
		setURL(URL);
	}

	public Agency(String ID, String name, String URL, String language) {
		this(ID, name, URL);
		setLanguage(language);
	}
	
	public Agency(String ID, String name, String URL, String language, String phoneNum){
		this(ID, name, URL, language);
		setPhoneNum(phoneNum);

	}
	
	public Agency(String ID, String name, String URL, String language, String phoneNum, String fareURL){
		this(ID, name, URL, language, phoneNum);
		setFareURL(fareURL);
	}

	//Full Constructor
	public Agency(String ID, String name, String URL, String language, String phoneNum, String fareURL, String email) {
		this(ID, name, URL, language, phoneNum, fareURL);
		setEmail(email);
	}
	
	/////////////
	// Methods //
	/////////////
	
	public boolean addRoute(Route routeToAdd){
		return routeList.add(routeToAdd);
	}

	
	///////////////////////
	// Getters & Setters //
    ///////////////////////
	
	/**
	 * Returns the agency ID
	 * @return ID
	 */
	public String getID() {
		return ID;
	}
	
	public ArrayList<Route>  getRouteList() {
		return routeList;
	}

	public void setRouteList(ArrayList<Route>  routeList) {
		this.routeList = routeList;
	}

	/**
	 * Sets an ID for the agency
	 * 
	 * @param ID the new ID
	 */
	public void setID(String ID) {
		this.ID = ID;
	}

	/**
	 * Returns the agencies name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the agencies name
	 * @param name The new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the agency URL
	 * 
	 * @return URL
	 */
	public String getURL() {
		return URL;
	}

	/**
	 * Sets a  URL for the agency
	 * 
	 * @param URL The new URL
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}

	/**
	 * Returns the agencies time zone
	 * 
	 * @return timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Sets the agencies time zone
	 * @param timeZone the new time-zone to be set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Returns the agencies language
	 * 
	 * @return language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the agencies language
	 * 
	 * @param language The new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Returns the agencies phone number
	 * 
	 * @return phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}

	/**
	 * Sets the agencies phoneNum
	 * 
	 * @param phoneNum The new phoneNum
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * Returns the URL to purchase tickets online
	 * 
	 * @return fareURL
	 */
	public String getFareURL() {
		return fareURL;
	}

	/**
	 * 
	 * Sets the agencies fareURL
	 * 
	 * @param fareURL the new URL
	 */
	public void setFareURL(String fareURL) {
		this.fareURL = fareURL;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public boolean equals(Object other){
		
		if(!(other instanceof Agency))
			return false;
		
		Agency that = (Agency) other;
		
		return this.ID.equals(that.ID)
			&& this.email.equals(that.email)
			&& this.name.equals(that.name);
	}
}
