package system;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.thalesgroup.rtti._2013_11_28.token.types.AccessToken;
import com.thalesgroup.rtti._2015_11_27.ldb.types.ServiceLocation;
import com.thalesgroup.rtti._2017_02_02.ldb.GetBoardRequestParams;
import com.thalesgroup.rtti._2017_02_02.ldb.LDBServiceSoap;
import com.thalesgroup.rtti._2017_02_02.ldb.Ldb;
import com.thalesgroup.rtti._2017_02_02.ldb.StationBoardResponseType;
import com.thalesgroup.rtti._2017_02_02.ldb.types.ArrayOfServiceItems;
import com.thalesgroup.rtti._2017_02_02.ldb.types.ServiceItem;
import com.thalesgroup.rtti._2017_02_02.ldb.types.StationBoard;

import classes.Agency;
import classes.Calendar;
import classes.CalendarDate;
import classes.Route;
import classes.Service;
import classes.Stop;
import classes.StopTime;
import classes.Trip;

public class Sys {

	private static BiMap<String, String> trainCodes;
	private static HashMap<String, Stop> trainStops;
	private static String token;
	private static ArrayList<Agency> agencyList;
	private static int stopsToInclude = 1000;

	public static void main(String[] args) {

		trainCodes = HashBiMap.create();
		trainStops = new HashMap<String, Stop>();
		token = "bab90793-8f56-4394-abac-7aaa0ba46aa7";
		agencyList = new ArrayList<Agency>();

		readGTFSData();
		printStops();

	}

	public static void printStops() {

		for (Agency agency : agencyList) {

			for (Route route : agency.getRouteList()) {
				System.out.println("Route:");
				for (Service service : route.getServiceList()) {
System.out.println("Service:");
					for (Trip trip : service.getTripList()) {
						System.out.println("Trip:");
						if (!trip.getStopsList().isEmpty()) {
							
//							System.out.println(service.getServiceID());
//							System.out.println(trip.getTripID());
							
//							String str = "";
							for (StopTime stopTime : trip.getStopsList()) {
//								if(str.length() > 0)
//								str = trip.getTripID() + " " + stopTime.getStop().getStopName();
//								
//								str = str + " " + stopTime.getArrivalTime();
							
							System.out.println(stopTime);
								
								
								//	System.out.println(stopTime.getArrivalTime());
							//	System.out.println(stopTime.getStop().getStopName() + " " + stopTime.getArrivalTime());

							}
					
							//System.out.println(str +"\n");
							
						}
					}
				}

			}
		}
	}

	public static void lbdAttempt() {

		Ldb ldb = new Ldb();
		LDBServiceSoap soap = ldb.getLDBServiceSoap();
		GetBoardRequestParams params = new GetBoardRequestParams();
		params.setCrs("CAR");

		AccessToken accessToken = new AccessToken();
		accessToken.setTokenValue(token);

		StationBoardResponseType response = soap.getDepartureBoard(params, accessToken);
		StationBoard sb = response.getGetStationBoardResult();

		System.out.println(sb.getLocationName());

		ArrayOfServiceItems serviceItems = sb.getTrainServices();

		List<ServiceItem> serviceItemList = serviceItems.getService();

		for (ServiceItem serviceItem : serviceItemList) {
			List<ServiceLocation> arrayOfService = serviceItem.getDestination().getLocation();

			for (ServiceLocation serviceLocation : arrayOfService) {
				System.out.println(serviceLocation.getLocationName());
			}
		}
	}

	public static void readGTFSData() {

		readCodes();
		//readFeedInfo();
		readAgency();
		readRoutes();
		readTrips();
		readStops();
		readStopTimes();
		readCalender();
		readCalenderDates();
	}

	private static void readAgency() {

		Scanner agencyScanner;
	//	System.out.println("Reading agencies");

		try {

			agencyScanner = new Scanner(new FileReader("GTFSdata/agency.txt"));
			// Skips first line
			agencyScanner.nextLine();

			while (agencyScanner.hasNext()) {

				Agency agency = new Agency(agencyScanner.nextLine());
				agencyList.add(agency);
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void readRoutes() {

		Scanner routeScanner;
	//	System.out.println("Reading Routes:");

		try {

			routeScanner = new Scanner(new FileReader("GTFSdata/routes.txt"));
			routeScanner.nextLine();

			while (routeScanner.hasNext()) {

				Route route = new Route(routeScanner.nextLine());
				if (!addRouteToAgency(route)) {
					System.out.println("Failure To Add Route "+route.getRouteID()+" To Agency " + route.getAgencyID());
				}

			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void readTrips() {

		Scanner tripScanner;
	//	System.out.println("Reading Trips:");

		try {

			tripScanner = new Scanner(new FileReader("GTFSdata/trips.txt"));
			tripScanner.nextLine();

			while (tripScanner.hasNext()) {

				Trip trip = new Trip(tripScanner.nextLine());
				if (!addTripToRoute(trip)) {
					System.out.println("Failure To Add Trip To Route");
				}

			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void readStopTimes() {

		Scanner stopTimeScanner;
		System.out.println("Reading Stop Times:");

		int count = 0;
		try {

			stopTimeScanner = new Scanner(new FileReader("GTFSdata/stop_times.txt"));
			stopTimeScanner.nextLine();

			while (stopTimeScanner.hasNext()) {

				

				String fileString = stopTimeScanner.nextLine();

				StopTime st = new StopTime(fileString);
				if (!addStopTimeToTrip(st)) {
					System.out.println("Failure To Add StopTime "+st.getStop().getStopId()+" To Trip: " + st.getTripId());
				}
				count++;

			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void readStops() {

		Scanner stopScanner;
		System.out.println("Reading Stops:");

		try {

			stopScanner = new Scanner(new FileReader("GTFSdata/stops.txt"));
			stopScanner.nextLine();

			while (stopScanner.hasNext()) {
				Stop stop = new Stop(stopScanner.nextLine());
				trainStops.put(stop.getStopId(), stop);
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static boolean addRouteToAgency(Route routeToAdd) {

		for (Agency agencyInstance : agencyList)
			if (agencyInstance.getID().equals(routeToAdd.getAgencyID()))
				return agencyInstance.addRoute(routeToAdd);

		return false;

	}

	private static boolean addTripToRoute(Trip tripToAdd) {

		for (Agency agencyInstance : agencyList)
			for (Route routeInstance : agencyInstance.getRouteList())
				if (routeInstance.getRouteID().equals(tripToAdd.getRouteID()))
					return routeInstance.addTrip(tripToAdd);

		return false;

	}

	private static boolean addStopTimeToTrip(StopTime stopTimeToAdd) {

		for (Agency agencyInstance : agencyList)
			for (Route routeInstance : agencyInstance.getRouteList())
				for (Trip tripInstance : routeInstance.getTripList()) {
					if (tripInstance.getTripID().equals(stopTimeToAdd.getTripId())) {
						return tripInstance.addStopTime(stopTimeToAdd);

					}
				}
		return false;

	}

	private static void addServiceToTrip(Service serviceToAdd) {

		for (Agency agencyInstance : agencyList) {
			for (Route routeInstance : agencyInstance.getRouteList()) {
				for (Trip tripInstance : routeInstance.getTripList()) {
					if (tripInstance.getServiceID().equals(serviceToAdd.getServiceID())) {
						tripInstance.setService(serviceToAdd);
						routeInstance.addService(serviceToAdd);
					}
				}
			}
		}

	}

	// Build trips for service
	
	private static void buildTripsForService(Service s) {
		
		for (Agency agencyInstance : agencyList) {
			for (Route routeInstance : agencyInstance.getRouteList()) {
				for (Trip tripInstance : routeInstance.getTripList()) {
					if (tripInstance.getServiceID().equals(s.getServiceID())) {
						s.addTrip(tripInstance);
					}
				}
			}
		}
		
	}

	private static void readCalenderDates() {

		Scanner calenderDateScanner;
		//System.out.println("Reading CalenderDates:");

		try {
			calenderDateScanner = new Scanner(new FileReader("GTFSdata/calendar_dates.txt"));

			calenderDateScanner.nextLine();

			while (calenderDateScanner.hasNext()) {
				CalendarDate calendarDate = new CalendarDate(calenderDateScanner.nextLine());

// Get Service 

				addCalendarDateToService(calendarDate);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static boolean addCalendarDateToService(CalendarDate calendarDate) {

		for (Agency agencyInstance : agencyList) {
			for (Route routeInstance : agencyInstance.getRouteList()) {
				for (Service serviceInstance : routeInstance.getServiceList()) {
					if (serviceInstance.getServiceID().equals(calendarDate.getServiceID())) {
						return serviceInstance.addCalendarDate(calendarDate);
					}
				}
			}
		}
		return false;

	}

	private static void readCalender() {

		Scanner calenderScanner;
	//	System.out.println("Reading Calender:");

		try {
			calenderScanner = new Scanner(new FileReader("GTFSdata/calendar.txt"));

			calenderScanner.nextLine();

			while (calenderScanner.hasNext()) {
				Calendar calendar = new Calendar(calenderScanner.nextLine());

				Service s = new Service(calendar.getServiceID());
				s.setCalender(calendar);
				addServiceToTrip(s);
				buildTripsForService(s);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void readCodes() {

		Scanner stationCodeScanner;
	//	System.out.println("Reading Codes:");

		try {

			stationCodeScanner = new Scanner(new FileReader("GTFSdata/station_codes.csv"));

			while (stationCodeScanner.hasNext()) {

				String[] splitter = stationCodeScanner.nextLine().split(",");
				trainCodes.put(splitter[0], splitter[1]);
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public static HashMap<String, Stop> getTrainStops() {
		return trainStops;
	}

	public static Stop getStopByCode(String code) {
		return trainStops.get(code);
	}

}
