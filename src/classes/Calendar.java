package classes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Calendar {

	String serviceID;
	boolean monday, tuesday, wednesday, thursday, friday, saturday, sunday;
	LocalDate startDate, endDate;
	
	public Calendar(String fileString){
		String[] split = fileString.split(",");
		this.serviceID = split[0];
		this.monday = parseBoolean(split[1]);
		this.tuesday = parseBoolean(split[2]);
		this.wednesday = parseBoolean(split[3]);
		this.thursday = parseBoolean(split[4]);
		this.friday = parseBoolean(split[5]);
		this.saturday = parseBoolean(split[6]);
		this.sunday = parseBoolean(split[7]);
		this.startDate = parseDate(split[8]);
		this.endDate = parseDate(split[9]);
		
	}
	
	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public boolean isSaturday() {
		return saturday;
	}

	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	public boolean isSunday() {
		return sunday;
	}

	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	/**
	 * Methods takes in a date as a string, checks if it is valid and then parses it to be returned as a localDate
	 * 
	 * @param dateInput The date as a string to be parsed
	 * @return date The date as a local date
	 */
	public LocalDate parseDate(String dateInput) {
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");

		if (!isValid(dateInput, format)) {
			System.out.println("Date Not Accepted");
			return null;
		}
		
		DateTimeFormatter newFormat = DateTimeFormatter.ofPattern("yyyyMMdd");

		
		LocalDate date = LocalDate.parse(dateInput, newFormat);
		return date;
	}

	/**
	 * Takes a date input and checks whether it fits the  criteria for the systems accepted format
	 * 
	 * @param input The date as a string to be checked
	 * 
	 * @return Boolean determining if the date is valid for the format
	 */
	public boolean isValid(String input,DateTimeFormatter format) {

		

		try {
			
			LocalDate.parse(input, format);
			return true;

		} catch (DateTimeParseException e) {
			return false;
		}

	}

	/**
	 * 
	 * @param input The boolean as a string
	 * @return A boolean value determining true of false
	 */
	private static boolean parseBoolean(String input){
		if (input == "1" || input.equalsIgnoreCase("true"))
			return true;
		
		if(input == "0" || input.equalsIgnoreCase("false"))
			return false;
		
		return false;
	}
		
}
