package classes;

import system.Console;
import system.Sys;

public class StopTime {
	
	private Stop stop;
	private String tripId;
	private String stopId;
	private String arrivalTime;
	private String departureTime;
	private String stopSequence;
	private int pickupType;
	private int dropOffType;

	
	public StopTime(String fileString) {
		String[] split = Console.parseCSVLine(fileString);

		setTripId(split[0]);
		setArrivalTime(split[1]);
		setDepartureTime(split[2]);
		setStopId(split[3]);
		setStopSequence(split[4]);
		if (split.length > 5) {
		setPickupType(split[5]);
		setDropOffType(split[6]);
		}
		
	}
	
	public StopTime(Stop stop, String tripId, String arrivalTime, String departureTime, String stopSequence, int pickupType, int dropOffType ) {
		
		setStop(stop);
		setTripId(tripId);
		setArrivalTime(arrivalTime);
		setDepartureTime(departureTime);
		setStopSequence(stopSequence);
		setPickupType(pickupType);
		setDropOffType(dropOffType);
		
	}
	
	public String toString() {
		return tripId + "\t" + stop.getStopName() + "\t\t\t" + arrivalTime + "\t";
	}

	public String getTripId() {
		return tripId;
	}



	public void setTripId(String tripId) {
		this.tripId = tripId;
	}



	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}
	
	public void setStopById(String id) {
		
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		
		this.departureTime = departureTime;
	}
	
	public String getStopSequence() {
		return stopSequence;
	}

	public void setStopSequence(String stopSequence) {
		this.stopSequence = stopSequence;
	}

	public int getPickupType() {
		return pickupType;
	}
	
	public void setPickupType(String pickupTypeAsString) {
		if (pickupTypeAsString == null)
			return;

		if (pickupTypeAsString.equals(""))
			return;

		setPickupType(Integer.parseInt(pickupTypeAsString));
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public int getDropOffType() {
		return dropOffType;
	}

	public void setDropOffType(String pickupTypeAsString) {
		if (pickupTypeAsString == null)
			return;

		if (pickupTypeAsString.equals(""))
			return;

		setDropOffType(Integer.parseInt(pickupTypeAsString));
	}
	
	public void setDropOffType(int dropOffType) {
		this.dropOffType = dropOffType;
	}

	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		 setStop(Sys.getStopByCode(stopId));
		this.stopId = stopId;
	}
	
	
	
}
