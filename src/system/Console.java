package system;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.util.regex.Pattern;


public class Console {

	private Scanner systemConsole;

	public Console() {
		this.systemConsole = new Scanner(System.in);
	}
	
	public static String[] parseCSVLine(String line) {
		// Create a pattern to match breaks
		Pattern p = Pattern.compile(",(?=([^\"]*\"[^\"]*\")*(?![^\"]*\"))");
		// Split input with the pattern
		String[] fields = p.split(line);
		for (int i = 0; i < fields.length; i++) {
			// Get rid of residual double quotes
			fields[i] = fields[i].replace("\"", "");
		}
		return fields;
	}

	/**
	 * Returns the systemConsole
	 * 
	 * @return systemConsole the Scanner for this class
	 */
	public Scanner getConsole() {
		return systemConsole;
	}

	/**
	 * Method for allowing users to input a line from the console
	 * 
	 * @return The next line input to console
	 */
	public String getNextLine() {
		return systemConsole.nextLine();
	}


	/**
	 * Directs the user to input a start date, then returns the parsed input as
	 * a localDate
	 * 
	 * @return the parsed date based on users input
	 */
	public LocalDate getStartDate() {
		System.out.println("Enter Start Date [HH:mm:ss]: ");
		return parseDate(getNextLine());
	}

	/**
	 * Directs the user to input an end date, then returns the parsed input as
	 * a localDate
	 * 
	 * @return the parsed date based on users input
	 */
	public LocalDate getEndDate() {
		System.out.println("Enter End Date [HH:mm:ss]: ");
		return parseDate(getNextLine());
	}

	////////////////
	// Real Dates //
	////////////////
	
	/**
	 * Methods takes in a date as a string, checks if it is valid and then parses it to be returned as a localDate
	 * 
	 * @param dateInput The date as a string to be parsed
	 * @return date The date as a local date
	 */
	public LocalDate parseDate(String dateInput) {
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");

		if (!isValid(dateInput, format)) {
			System.out.println("Date Not Accepted");
			return null;
		}
		
		DateTimeFormatter newFormat = DateTimeFormatter.ofPattern("yyyyMMdd");

		
		LocalDate date = LocalDate.parse(dateInput, newFormat);
		return date;
	}

	/**
	 * Takes a date input and checks whether it fits the  criteria for the systems accepted format
	 * 
	 * @param input The date as a string to be checked
	 * 
	 * @return Boolean determining if the date is valid for the format
	 */
	public boolean isValid(String input,DateTimeFormatter format) {

		

		try {
			
			LocalDate.parse(input, format);
			return true;

		} catch (DateTimeParseException e) {
			return false;
		}

	}

}
