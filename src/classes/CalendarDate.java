package classes;

import system.Console;

public class CalendarDate {

	String serviceID;
	String date;
	String exceptionType;
	
	public CalendarDate(String fileString) {
		String[] split = Console.parseCSVLine(fileString);
		setServiceID(split[0]);
		setDate(split[1]);
		setExceptionType(split[2]);
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}
	
}
