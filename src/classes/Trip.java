package classes;

import java.util.ArrayList;

import system.Console;

public class Trip {

	private ArrayList<StopTime> stopsList = new ArrayList<StopTime>();
	private Service service;
	private String routeID;
	private String serviceID;
	private String tripID;
	private String tripHeadSign;
	
	public Trip(String fileString){
		String[] split = Console.parseCSVLine(fileString);
		setRouteID(split[0]);
		setServiceID(split[1]);
		setTripID(split[2]);
		setTripHeadSign(split[3]);
	}
	
	public void setService(Service serviceToAdd) {
		this.service = serviceToAdd;
	}
	
	public Service getService() {
		return this.service;
	}

	public String toString() {		
		return tripHeadSign;
	}
	
	public ArrayList<StopTime> getStopsList() {
		return stopsList;
	}

	public void setStopsList(ArrayList<StopTime> stopsList) {
		this.stopsList = stopsList;
	}

	public String getRouteID() {
		return routeID;
	}

	public void setRouteID(String routeID) {
		this.routeID = routeID;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		
		this.serviceID = serviceID;
		
	}

	public String getTripID() {
		return tripID;
	}
	
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}
	
	public String getTripHeadSign() {
		return tripHeadSign;
	}
	
	public void setTripHeadSign(String tripHeadSign) {
		this.tripHeadSign = tripHeadSign;
	}

	public boolean addStopTime(StopTime stopTimeToAdd) {
		return stopsList.add(stopTimeToAdd);
	}
	
}
